package com.zhou;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class RedisUtil {

    private static RedisUtil instance = null;

    private static JedisPool jedisPool = null;

    public static RedisUtil getInstance() {
        return instance == null ? new RedisUtil() : instance;
    }

    public RedisUtil() {
        if (jedisPool == null) {
            String ip = "192.168.20.246";
            int port = 7589;
            String password = "66C3D88E3F8842D7BF7040F651540AC0";
            JedisPoolConfig config = new JedisPoolConfig();
            config.setMaxTotal(10000);
            config.setMaxIdle(10000);
            config.setMaxWaitMillis(10000);
            jedisPool = new JedisPool(config, ip, port, 0, password);
        }
    }

    public Jedis getJedis() {
        Jedis jedis = jedisPool.getResource();
        return jedis;
    }

    public void returnJedis(Jedis jedis){
        jedisPool.returnResourceObject(jedis);
    }
}
