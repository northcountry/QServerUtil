package com.zhou;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.Transaction;

import java.util.List;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class RedisLock {
    public static final String clientName = "clientName";

    public static void main(String[] args) {
//        Jedis jedis = RedisUtil.getInstance().getJedis();
//        jedis.set("hello", "999");
//
//        System.out.println(jedis.get("hello"));
        String name = "big baby";
        initProduct(name, 10);
        initClient(name);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        printresult(clientName);

    }

    public static void printresult(String goodsName) {
        Jedis jedis = RedisUtil.getInstance().getJedis();
        Set<String> smembers = jedis.smembers(goodsName);
        for (String value : smembers) {
            System.out.println(goodsName + "抢购成功:" + value);
        }

    }

    /**
     * 初始商品订单
     *
     * @param goodsName
     * @param num
     */
    public static void initProduct(String goodsName, int num) {
        Jedis jedis = RedisUtil.getInstance().getJedis();
        if (jedis.exists(goodsName)) {
            jedis.del(goodsName);
        }
        if (jedis.exists(clientName)) {
            jedis.del(clientName);
        }
        jedis.set(goodsName, String.valueOf(num));
    }

    public static void initClient(String goodsName) {
        ThreadPoolExecutor poolExecutor = new ThreadPoolExecutor(10, 13, 1000, TimeUnit.MILLISECONDS, new ArrayBlockingQueue<Runnable>(100000));
        for (int i = 0; i < 1000; i++) {
            poolExecutor.execute(new ClientBuy(goodsName, i + 1));
        }
        poolExecutor.shutdown();
    }
}

class ClientBuy implements Runnable {

    String userame;
    String goodsName;

    public ClientBuy(String keyName, int num) {
        this.userame = "client:" + num;
        this.goodsName = keyName;
    }

    @Override
    public void run() {
        Jedis jedis = RedisUtil.getInstance().getJedis();
        while (true) {
            System.out.println(userame + ":开始抢购...");
            jedis.watch(goodsName);
            int relaseNum = Integer.parseInt(jedis.get(goodsName));
            if (relaseNum > 0) {
                Transaction multi = jedis.multi();
                multi.set(goodsName, String.valueOf(relaseNum - 1));
                List<Object> exec = multi.exec();
                if (exec == null || exec.isEmpty()) {
                    System.out.println(userame + ":提交订单中,请稍后...");
                } else {
                    jedis.sadd("clientName", userame);
                    System.out.println(userame + ":可喜可贺,购买成功");
                    break;
                }
            } else {
                System.out.println(userame + ":sorry,商品被抢夺完,我们下次再见");
                break;
            }
        }

    }
}
