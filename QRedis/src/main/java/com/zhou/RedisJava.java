package com.zhou;

import redis.clients.jedis.*;

import javax.xml.bind.SchemaOutputResolver;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class RedisJava {
    public static void main(String[] args) {
        Jedis jedis = new Jedis("127.0.0.1", 6379);
        System.out.println("ping:" + jedis.ping());
        String value = jedis.get("key");
        System.out.println(value);
        System.out.println(jedis.incr("key"));
        System.out.println(jedis.decr("key"));
        System.out.println(jedis.decr("key"));
        System.out.println(jedis.decr("key"));
        System.out.println(jedis.get("key"));

        jedis.lpush("list-key", "64");
        jedis.lpush("list-key", "23");
        jedis.rpush("list-key", "zhou");
        System.out.println("------------" + jedis.lrange("list-key", 0, -1));
        System.out.println(jedis.sort("list-key"));
//        jedis.sort("list-key",1);
        Pipeline pipelined = jedis.pipelined();
        jedis.expire("list-key", 100);
        try {
            TimeUnit.SECONDS.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("............." + jedis.lrange("list-key", 0, -1));
        System.out.println(jedis.ttl("list-key"));

//        jedis.zadd("zadd",34,"zhou");
//        jedis.zadd("zadd",32,"zhou1");
//        jedis.zadd("zadd",1,"zhou2");
//        System.out.println(jedis.zcard("zadd"));
//
//        String str ="message test";
//        jedis.publish("nettychannel",str);
        //jedis.subscribe();

        /** 发布 订阅模式*/
//        String ip = "127.0.0.1";
//        int port = 6379;
//
//        JedisPool jedisPool = new JedisPool(new JedisPoolConfig(),ip,port);
//        System.out.println(String.format("redispool is connected,ip %s,port %d",ip,port));
//        SubcriberThread subcriberthread = new SubcriberThread(jedisPool);
//        subcriberthread.start();
//
//        for(int i =0;i<100;i++) {
//            SubcriberThread1 subcriberThread1 = new SubcriberThread1(jedisPool);
//            subcriberThread1.start();
//        }
//
//        for(int i = 0;i< 100;i++) {
//            Publisher publisher = new Publisher(jedisPool);
//            publisher.start();
//        }
    }
}

class Subcriber extends JedisPubSub {
    public void onMessage(String channel, String message) {
        System.out.println(Thread.currentThread() + String.format(";recieve redis published message,channel %s,message %s", channel, message));
    }

    @Override
    public void onSubscribe(String channel, int subscribedChannels) {
        System.out.println("subcribe channel:" + channel + ",subscribeedChannled:" + subscribedChannels);
    }

    @Override
    public void onUnsubscribe(String channel, int subscribedChannels) {
        System.out.println("onUnsubscribe channel:" + channel + ",unsubscribeedChannled:" + subscribedChannels);
    }
}

class SubcriberThread extends Thread {
    private final JedisPool jedisPool;
    private Subcriber subcriber = new Subcriber();
    private final String channel = "mychannel";

    public SubcriberThread(JedisPool jedisPool) {
        super("Subcriber");
        this.jedisPool = jedisPool;
    }

    public void run() {
        System.out.println("chanel:" + channel);
        Jedis jedis = null;
        jedis = jedisPool.getResource();
        jedis.subscribe(subcriber, channel);
    }
}

class SubcriberThread1 extends Thread {
    private final JedisPool jedisPool;
    private Subcriber subcriber = new Subcriber();
    private final String channel = "mychannel";

    public SubcriberThread1(JedisPool jedisPool) {
        super("Subcriber");
        this.jedisPool = jedisPool;
    }

    public void run() {
        System.out.println("chanel:" + channel);
        Jedis jedis = null;
        jedis = jedisPool.getResource();
        jedis.subscribe(subcriber, channel);
    }
}

class Publisher {
    private final JedisPool jedisPool;
    private final String channel = "mychannel";

    public Publisher(JedisPool jedisPool) {
        this.jedisPool = jedisPool;
    }

    public void start() {
        Random r = new Random();
        int a = r.nextInt(1000);
        Jedis jedis = jedisPool.getResource();
        String test = "创建发布线程和订阅线程，分别做一下的事情\n" +
                "\n" +
                "  jedis.publish(channel,content);//发布\n" +
                "\n" +
                "  jedis.subscribe(subcriber,channel);//订阅\n" +
                "\n" +
                "在使用redis的发布和订阅的时候，有两个问题：\n" +
                "\n" +
                "（a）当数据过大，redis 的吞吐量可能不足以keep a large outgoing buffer,数据过大可能导致redis 变的非常慢，甚至是crash,系统会自动杀掉redis,";
        while (true) {
            for (int i = 0; i < 1000; i++) {
                jedis.publish(channel, test);
            }
        }

//            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
//            Jedis jedis = jedisPool.getResource();
//
//            while(true){
//                Random r = new Random();
//                int a =  r.nextInt(1000);
//                String line = null;
//                try {
//                    line = reader.readLine();
//                    if(!"quit".equals(line)){
//                        jedis.publish(channel,line);
//                    }
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
    }

}

