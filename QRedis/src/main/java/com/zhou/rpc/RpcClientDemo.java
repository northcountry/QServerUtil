package com.zhou.rpc;

import com.alibaba.fastjson.JSON;

import java.io.IOException;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.util.Iterator;

public class RpcClientDemo {
    private static SocketChannel channel;
    private static Selector selector;
    ByteBuffer buffer = ByteBuffer.allocate(1024);

    private static RpcClientDemo instance = new RpcClientDemo();

    public static RpcClientDemo getInstance() {
        return instance;
    }

    public static void main(String[] args) {
        RpcClientDemo client = RpcClientDemo.getInstance();
        try {
            client.init("127.0.0.1", 9000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Hello hello = (Hello) client.getClassProxy(Hello.class);
        //String res = hello.say("yoyo ");
        //System.out.println(res);
        Long obj = Long.parseLong(String.valueOf(hello.getNum()));
        System.out.println(obj);
    }

    public RpcClientDemo init(String ip, int port) throws Exception {
        selector = Selector.open();
        InetSocketAddress isa = new InetSocketAddress(ip, port);
        // 获取socket通道
        channel = SocketChannel.open(isa);
        // 连接服务器
        channel.configureBlocking(false);
        channel.register(selector, SelectionKey.OP_READ);
        System.out.println("客户端启动成功");
        return this;
    }

    public Object getClassProxy(final Class clazz) {
        return Proxy.newProxyInstance(clazz.getClassLoader(), new Class[]{clazz}, new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                RpcProtocol rpcProtocol = new RpcProtocol();
                rpcProtocol.setClassName(clazz.getName());
                rpcProtocol.setMethodName(method.getName());
                if (method.getParameterTypes() != null) {
                    rpcProtocol.setParameterTypes(method.getParameterTypes());
                    rpcProtocol.setArguments(args);
                }
                System.out.println(6666666);
                channel.write(ByteBuffer.wrap(JSON.toJSONString(rpcProtocol).getBytes("UTF-8")));
                Object result = getChannelResult();
                return result;
            }
        });
    }

    public Object getChannelResult() throws IOException {
        while (true) {
            int select = selector.select();
            if (select > 0) {
                Iterator<SelectionKey> iterable = selector.selectedKeys().iterator();
                while (iterable.hasNext()) {
                    SelectionKey key = iterable.next();
                    if (key.isReadable()) {
                        SocketChannel channel = (SocketChannel) key.channel();
                        StringBuilder stringBuilder = new StringBuilder();
                        String message = null;
                        int byteread = channel.read(buffer);
                        if (byteread != 0) {
                            int position = buffer.position();
                            byte[] data = buffer.array();
                            message = new String(data, 0, position);
                            buffer.clear();
                        }
                        return message;
                    }
                }
            }
        }
    }
}
