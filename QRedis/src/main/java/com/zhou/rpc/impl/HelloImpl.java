package com.zhou.rpc.impl;

import com.zhou.rpc.Hello;

public class HelloImpl implements Hello {
    @Override
    public String say(String words) {
        return "服务器返回语句:" + words;
    }

    @Override
    public Long getNum() {
        return System.currentTimeMillis();
    }
}
