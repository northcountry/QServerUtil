package com.zhou.rpc;

public interface Hello {
    public String say(String words);

    public Long getNum();
}
