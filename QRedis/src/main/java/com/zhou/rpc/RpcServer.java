package com.zhou.rpc;

import com.alibaba.fastjson.JSONObject;
import com.zhou.rpc.impl.HelloImpl;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import static com.zhou.rpc.RpcServer.exportService;

public class RpcServer<T> {
    public static Map<String, Object> exportService = new ConcurrentHashMap<>();

    public static void main(String[] args) {
        Hello hello = new HelloImpl();
        exportService.put(Hello.class.getName(), hello);
        RpcServerStart(9000);
        String message = "{\"arguments\":[\"hello\"],\"className\":\"com.zhou.rpc.Hello\",\"methodName\":\"say\",\"parameterTypes\":[\"java.lang.String\"]}";

        System.out.println(1);
    }

    /**
     * 处理请求线程池
     */
    private static ThreadPoolExecutor executorPool = new ThreadPoolExecutor(10, 20, 1000, TimeUnit.MILLISECONDS, new ArrayBlockingQueue<Runnable>(1024));

    public static void RpcServerStart(int port) {
        ServerSocketChannel socketChannel = null;
        try {
            socketChannel = ServerSocketChannel.open();
            socketChannel.socket().bind(new InetSocketAddress(port));
            Selector selector = Selector.open();
            socketChannel.configureBlocking(false);
            socketChannel.register(selector, SelectionKey.OP_ACCEPT);
            System.out.println("服务器 已经启动");
            while (true) {
                int select = selector.select();
                if (select <= 0) {
                    continue;
                }
                Iterator<SelectionKey> iterable = selector.selectedKeys().iterator();
                while (iterable.hasNext()) {
                    SelectionKey key = iterable.next();
                    if (key.isAcceptable()) {
                        SocketChannel channel = ((ServerSocketChannel) key.channel()).accept();
                        channel.configureBlocking(false);
                        channel.register(selector, SelectionKey.OP_READ);
                    }
                    if (key.isReadable()) {
                        SocketChannel channel = (SocketChannel) key.channel();
                        executorPool.execute(new DataProcess(channel));
                    }
                    iterable.remove();
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

class DataProcess implements Runnable {

    private SocketChannel channel;

    public DataProcess(SocketChannel channel) {
        this.channel = channel;
    }

    @Override
    public void run() {
        ByteBuffer buffer = ByteBuffer.allocate(1024 * 16);
        try {
            StringBuilder stringBuilder = new StringBuilder();
            String message = null;
            int byteread = channel.read(buffer);
            if (byteread != 0) {
                int position = buffer.position();
                byte[] data = buffer.array();
                message = new String(data, 0, position);
                buffer.clear();
            } else {
                return;
            }

//            while (byteread != -1) {
//                int position = buffer.position();
//                if (position == 0) {
//                    break;
//                }
//                byte[] data = buffer.array();
//                //buffer.flip();
//                stringBuilder.append(new String(data, 0, position));
//                buffer.clear();
//                byteread = channel.read(buffer);
//            }
            System.out.println(message);
            RpcProtocol protocol = JSONObject.parseObject(message, RpcProtocol.class);
//            Class service = Class.forName(protocol.getClassName());
//           // Class c = Proxy.newProxyInstance(service.getClassLoader(), new Class[]{service}, new ProxyHandler(new Student()));
//            //Person p = (Person) Proxy.newProxyInstance(Person.class.getClassLoader(), new Class[]{Person.class}, new ProxyHandler(new Student()));
//            Method method = service.getMethod(protocol.getMethodName(), protocol.getParameterTypes());
//            Object result = method.invoke(service.newInstance(), protocol.getArguments());
            Class service = Class.forName(protocol.getClassName());
            Object target = exportService.get(protocol.getClassName());
            if (target == null) {
                System.out.println("没找到调用");
                return;
            }
            Method method = service.getMethod(protocol.getMethodName(), protocol.getParameterTypes());
            Object result = method.invoke(target, protocol.getArguments());
            System.out.println(result);
            channel.write(ByteBuffer.wrap(result.toString().getBytes("utf-8")));
            channel.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
