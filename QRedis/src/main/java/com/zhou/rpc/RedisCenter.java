package com.zhou.rpc;

import com.zhou.RedisUtil;
import redis.clients.jedis.Jedis;

import java.util.HashSet;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class RedisCenter {
    private static final String interfaceKey = "Inter";
    private static Set<ExecutorService> sets = new HashSet<>();
    public static void register() {
        RedisUtil instance = RedisUtil.getInstance();
        Jedis jedis = instance.getJedis();
        String result = jedis.get("key");
        System.out.println(result);
    }

    public static void main(String[] args) {
        double p = Math.pow(1.002,100);
        System.out.println(p);
        register();

        for(int i = 0;i< 1000;i++){
            sets.add(Executors.newSingleThreadExecutor());
        }
    }
}
