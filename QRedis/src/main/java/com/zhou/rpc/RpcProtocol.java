package com.zhou.rpc;

import com.alibaba.fastjson.JSON;

public class RpcProtocol {
    private String className;
    private String methodName;
    private Class<?>[] parameterTypes;
    private Object[] arguments;
    private Object result;

    public byte[] getBytes() {
//        String message = JSON.toJSONString(this);

//        return message.getBytes();
        return null;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public Class<?>[] getParameterTypes() {
        return parameterTypes;
    }

    public void setParameterTypes(Class<?>[] parameterTypes) {
        this.parameterTypes = parameterTypes;
    }

    public Object[] getArguments() {
        return arguments;
    }

    public void setArguments(Object[] arguments) {
        this.arguments = arguments;
    }
}