package com.zhou.proxydemo;

public interface Person {
    int getAge();

    String getName();
}
