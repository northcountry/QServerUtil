package com.zhou.proxydemo;

public class StaticProxy implements Person{

    private Person  person;

    public StaticProxy(Person person){
        this.person = person;
    }

    public int getAge() {
        return person.getAge();
    }

    public String getName() {
        return person.getName();
    }

}
