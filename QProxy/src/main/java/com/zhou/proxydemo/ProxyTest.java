package com.zhou.proxydemo;

import sun.misc.ProxyGenerator;

import java.io.FileOutputStream;
import java.lang.reflect.Proxy;

public class ProxyTest {
    public static void main(String[] args) {
        dynamicProxyDemo();
        staticProxyDemo();
    }

    /**
     * 动态代理  只能代理含有接口得类,没有接口得不能成功
     */
    private static void dynamicProxyDemo() {
        System.out.println(1);
        Person p = (Person) Proxy.newProxyInstance(Person.class.getClassLoader(),new Class[]{Person.class},new ProxyHandler(new Student()));
        int age  =  p.getAge();
        System.out.println(age);

        /** 生成 code */
        byte[] arrayOfByte = ProxyGenerator.generateProxyClass("$Proxy0", new Class[]{Person.class});
        Class localClass1 = null;
        String path = "E:\\sanguo\\NIOlearn\\bin\\StuProxy.class";
        try{
            FileOutputStream fos = new FileOutputStream(path);
            fos.write(arrayOfByte);
            fos.flush();
            System.out.println("代理类class文件写入成功");
        } catch (Exception e) {
            System.out.println("写文件错误");
        }
    }


    public static void  staticProxyDemo(){
        StaticProxy staticProxy  =new StaticProxy(new Student());
        int age = staticProxy.getAge();
        String name = staticProxy.getName();
        System.out.println("age:" + age + ",name:" + name);
    }
}
