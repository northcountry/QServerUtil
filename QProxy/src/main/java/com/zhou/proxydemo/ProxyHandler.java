package com.zhou.proxydemo;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class ProxyHandler implements InvocationHandler{

    private Object object;

    public ProxyHandler(Object object){
        this.object = object;
    }

    /**
     * 目标类得 所有方法执行都是通过这里执行得
     * @param proxy
     * @param method
     * @param args
     * @return
     * @throws Throwable
     */
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Object result = method.invoke(object,args);
        return result;
    }
}
