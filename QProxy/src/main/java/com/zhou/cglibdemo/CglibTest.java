package com.zhou.cglibdemo;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.InterfaceMaker;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * cglib 代理相对于proxy ,不需要代理类必须是接口,底层封装得是asm 字节码生成器;
 */
public class CglibTest {
    public static void main(String[] args) {
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(Star.class);
        enhancer.setCallback(new MethodInterceptor() {
            @Override
            public Object intercept(Object obj, Method method, Object[] args, MethodProxy methodProxy) throws Throwable {
                System.out.println("before method run....");
                Object result = methodProxy.invokeSuper(obj,args);
                System.out.println("after run method..");
                return result;
            }
        });

        Star star = (Star) enhancer.create();
        star.act();


        InterfaceMaker interfaceMaker = new InterfaceMaker();
        interfaceMaker.add(Star.class);

        Class<?> targetInterface = interfaceMaker.create();
        for(Method method:targetInterface.getMethods()){
            System.out.println(method.getName());
        }

        Object object = Enhancer.create(Object.class, new Class[]{targetInterface}, new MethodInterceptor() {
            @Override
            public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
                if(method.getName().equals("act")){
                    return "6666";
                }
                return "default";
            }
        });

    }
}
