package com.zhou;

import dao.PersonMapper;
import dao.UserMapper;
import module.Person;
import module.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import sun.security.provider.MD5;

import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

public class Demo {

    public static void main(String[] args) {
        InputStream inputStream = null;
        SqlSessionFactory factory = null;
        SqlSession session = null;

        try {
            inputStream = Resources.getResourceAsStream("Configuration.xml");
            factory = new SqlSessionFactoryBuilder().build(inputStream);
            session = factory.openSession();


            UserMapper userDAO = session.getMapper(UserMapper.class);
            User user  = new User();
            user.setUid(UUID.randomUUID().toString().substring(1, 10));
            user.setUsername("adm6in");
            user.setPassword("127456");
            userDAO.insert(user);

            System.out.println("success");
            PersonMapper personMapper = session.getMapper(PersonMapper.class);
            Person p =new Person();
            p.setId("edfddfg");
            p.setSkill("sksd");
            personMapper.insert(p);
            System.out.println("success person");



        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
