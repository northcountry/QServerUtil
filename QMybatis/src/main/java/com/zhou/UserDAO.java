package com.zhou;

public interface UserDAO {
    public  User getUser(String uid);

    public void insertUser(User user);

}
