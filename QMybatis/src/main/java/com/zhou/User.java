package com.zhou;

import lombok.Data;

import java.io.Serializable;

/**
 * 用户信息
 */
@Data
public class User implements Serializable {
    private String uid;
    private String username;
    private String password;
}
