package dao;

import module.Person;

public interface PersonMapper {
    int insert(Person record);

    int insertSelective(Person record);
}