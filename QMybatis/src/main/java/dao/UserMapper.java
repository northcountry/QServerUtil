package dao;

import module.User;

public interface UserMapper {
    int insert(User record);

    int insertSelective(User record);
}