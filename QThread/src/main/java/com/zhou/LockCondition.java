package com.zhou;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class LockCondition {
    Lock lock =  new ReentrantLock();
    Condition condition = lock.newCondition();
    /**
     * 调用该方法 表示当前线程释放掉锁  并在此处等待
     */
    public void conditionAwait(){
        lock.lock();
        try {
            condition.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        finally {
            lock.unlock();
        }
    }
    /**
     * 其他线程调用该signal()，通知当前线程从wait()中返回，并且返回前已经获取到了锁
     */
    public void conditionSignale(){
        lock.lock();
        try {
            condition.signal();
        }finally {
            lock.unlock();
        }
    }

    public static void main(String[] args) {
        Map p = new HashMap<String,String>();
        p.put("2dsf","wer");
    }
}
