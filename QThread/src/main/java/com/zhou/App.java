package com.zhou;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );


        final Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 10; i++) {
                    System.out.println(Thread.currentThread() + " is  start");
                }
            }
        });
        thread.setName("hahah thread");
        System.out.println(Thread.currentThread());
        thread.start();
        System.out.println(Thread.currentThread());
    }
}
