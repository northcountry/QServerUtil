package com.zhou;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadPoolExectorSerivice {

    /** 确定的线程的 线程池*/
  static    ExecutorService fixedService = Executors.newFixedThreadPool(10);

    public static void main(String[] args) {
        System.out.println(fixedService.isTerminated());
        fixedService.shutdownNow();
        System.out.println(fixedService.isTerminated());
        fixedService.shutdown();
        System.out.println(fixedService.isTerminated());

    }

    public void fixedThreadPool(){

        Thread t = new Thread();
        t.start();
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {

            }
        });
        fixedService.submit(new Runnable() {
            @Override
            public void run() {

            }
        });
    }
}
