package com.zhou.dubbo;

import com.alibaba.dubbo.config.annotation.Service;

import java.util.ArrayList;
import java.util.List;

@Service(version = "2.5.3",
application = "demo-provider",
protocol = "dubbo",
 registry = "zookeeper://localhost:2181")
public class DemoServiceImpl implements  DemoService {
    @Override
    public List<String> gePermission(Long id) {
        List<String> demos  = new ArrayList<>();
        demos.add(String.valueOf(id -1));
        demos.add(String.valueOf(id));
        demos.add(String.valueOf(id + 1));
        return demos;
    }
}
