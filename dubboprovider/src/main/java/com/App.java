package  com;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;

public class App {
    public static void main(String[] args) throws IOException {

        System.out.println("dubbo provider start");

        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("provider.xml");

        System.out.println(context.getDisplayName() + ":here");
        context.start();
        System.out.println("service started ...");
        System.in.read();
    }
}
