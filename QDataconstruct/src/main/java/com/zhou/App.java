package com.zhou;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        //Object[] element
        List<String> list = new ArrayList <>();
        list.add("ho");

        //Node{Object date,node pre,node next}
        List<Integer> linekedList = new LinkedList <>();
        linekedList.add(77);

        //数组 + 链表    Node[] + Node 链表
        Map<String,String> map = new HashMap<String,String>();
        map.put("key","value");

        Map<String,String> concurrentHashMap =new ConcurrentHashMap<String,String>();
        concurrentHashMap.put("key1","value1");


    }
}
class Node{
    final int hash = 0;
    Object key;
    Object value;
    Node next;
}
