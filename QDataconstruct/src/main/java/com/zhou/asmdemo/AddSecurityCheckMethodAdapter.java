package com.zhou.asmdemo;

import com.sun.xml.internal.ws.org.objectweb.asm.MethodAdapter;
import com.sun.xml.internal.ws.org.objectweb.asm.MethodVisitor;
import jdk.internal.org.objectweb.asm.Opcodes;
import jdk.nashorn.internal.runtime.regexp.joni.constants.OPCode;


public class AddSecurityCheckMethodAdapter extends MethodAdapter {
    public AddSecurityCheckMethodAdapter(MethodVisitor mv) {
        super(mv);
    }

    public void visitCode(){
        visitMethodInsn(Opcodes.INVOKESTATIC,"SecurityChecker","checkSecurity","()v");
    }
}
