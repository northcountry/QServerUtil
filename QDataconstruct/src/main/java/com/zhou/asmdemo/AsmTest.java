package com.zhou.asmdemo;

import com.sun.xml.internal.ws.org.objectweb.asm.*;
import jdk.internal.org.objectweb.asm.ClassReader;
import jdk.internal.org.objectweb.asm.ClassWriter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class AsmTest {
    public static void main(String[] args) throws IOException {
        ClassReader classReader = new ClassReader("Account");

        ClassWriter classWriter = new ClassWriter(ClassWriter.COMPUTE_MAXS);

        ClassAdapter classAdapter = new AddSecurityCheckClassAdapter((ClassVisitor) classWriter);


        ClassVisitor visitor = new ClassVisitor() {
            @Override
            public void visit(int i, int i1, String s, String s1, String s2, String[] strings) {

            }

            @Override
            public void visitSource(String s, String s1) {

            }

            @Override
            public void visitOuterClass(String s, String s1, String s2) {

            }

            @Override
            public AnnotationVisitor visitAnnotation(String s, boolean b) {
                return null;
            }

            @Override
            public void visitAttribute(Attribute attribute) {

            }

            @Override
            public void visitInnerClass(String s, String s1, String s2, int i) {

            }

            @Override
            public FieldVisitor visitField(int i, String s, String s1, String s2, Object o) {
                return null;
            }

            @Override
            public MethodVisitor visitMethod(int i, String s, String s1, String s2, String[] strings) {
                return null;
            }

            @Override
            public void visitEnd() {

            }
        };

        classReader.accept((jdk.internal.org.objectweb.asm.ClassVisitor) visitor,ClassReader.SKIP_DEBUG);

        byte[] bytes = classWriter.toByteArray();

        File file = new File("Account.class");

        FileOutputStream out = new FileOutputStream(file);

        out.write(bytes);

        out.flush();


        Account account = new Account();
        account.operation();
    }
}
