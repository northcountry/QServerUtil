package com.zhou.asmdemo;

import com.sun.xml.internal.ws.org.objectweb.asm.ClassAdapter;
import com.sun.xml.internal.ws.org.objectweb.asm.ClassVisitor;
import com.sun.xml.internal.ws.org.objectweb.asm.MethodVisitor;

public class AddSecurityCheckClassAdapter extends ClassAdapter {
    public AddSecurityCheckClassAdapter(ClassVisitor cv) {
        super(cv);
    }

    public MethodVisitor visitMethod(final int access,final String name,final String desc,final String signature,
                                     final String[] exceptiones){
        MethodVisitor methodVisitor = cv.visitMethod(access,name,desc,signature,exceptiones);

        MethodVisitor wrappedMv = methodVisitor;

        if(methodVisitor !=null){
            if(name.equals("operation")){
                wrappedMv = new AddSecurityCheckMethodAdapter(wrappedMv);
            }
        }
        return null;
    }
}
