package com.zhou.TreeDemo;

import java.util.ArrayList;
import java.util.List;

/***
 B树  balance tree :多路平衡树


 m叉树
 1，每个节点最多有m个分支，就是m个子树 ，B树的最下面一层都存放的都是null（没有表现出出来），
 所以以最下一层分支为基准；非根节点至少有ceil（M/2）个分支
 2，每个节点上都存有不止一个关键字（ceil（M/2） -1），以及指向子节点的指针，如果一个节点有n个关键字，那么就有n+1 个分支;
 并且，关键字 都是依次递增的存放
 3，叶子节点都处于同一层
 */
public class BTree {
    static BNode root = null;

    public static void main(String[] args) {
        BTree bTree = new BTree();
        if (root == null) {
            root = new BNode(5);
        }
        bTree.insert(root, 1);
    }

    void insert(BNode bNode, int data) {
        //小于 最大关键字个数
        if (bNode.getKeyPointSize() < bNode.degree) {
            bNode.addKeyPoint(bNode.getKeyPointSize(), bNode);
        }
    }

    void split(BNode bNode) {
    }

    void delete(int data) {

    }
}

class BNode<E> {
    //阶层
    int degree;
    // 关键字
    List<E> keyPoint = new ArrayList<>(degree - 1);
    // 子节点
    List<BNode> childNode = new ArrayList<>(degree);
    //子节点
    private boolean leaf;

    BNode() {
    }

    BNode(int degree) {
        this.degree = degree;
    }
    public  int getKeyPointSize(){
        return keyPoint.size();
    }

    public boolean isLeaf(){
        return leaf;
    }


    public int addKeyPoint(int index, E value) {
        return index;
    }

    public void addChildNode(int index, BNode node) {

    }

    public void removeKeyPoint(int index) {

    }

    public void removeChildNode(int index, BNode node) {

    }
}