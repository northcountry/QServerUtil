package com.zhou;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class ArrayDemo {
    public static void main(String[] args) {
//        System.out.println(new int[]{1,5,6});
//        List a  = Arrays.asList(new int[]{234,345,456});
//        System.out.println(new ArrayList<>());
//        System.out.println(a.size());
//        Thread t = new Thread();
//        t.getThreadGroup();
        final ThreadLocal<ThreadData> threadLocals= new ThreadLocal<>();

        threadLocals.set(new ThreadData());
        threadLocals.set(new ThreadData(1));
        threadLocals.set(new ThreadData(2));
        ThreadData data =  threadLocals.get();
        System.out.println(data.a);
        Thread thread = Thread.currentThread();
        System.out.println(1);
        Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                threadLocals.set(new ThreadData());
                ThreadData data =  threadLocals.get();
                System.out.println("Thread2");
            }
        });
        thread1.start();
        Thread mainthread = Thread.currentThread();
        System.out.println(1);
        Vector v= new Vector();
        v.add(new ThreadData());
        v.add(new Integer(12));
        System.out.println(v);
        for(Object o:v){
            System.out.println(o);
        }
        System.out.println(v.elementAt(1));
        Enumeration en  = v.elements();
        while (en.hasMoreElements()){
            en.nextElement();
        }


        BitSet set = new BitSet();
       // set.and();

        Stack stack = new Stack();

        stack.push(new ThreadData(1));
        stack.push(new ThreadData(5));
        stack.add(1,new ThreadData(12));

//        Dictionary dictionary= new Dictionary() {
//        };

        Hashtable table = new Hashtable<>();
        table.put(1,1);

        Set s  = new HashSet();
        s.add(new ThreadData(23));
        s.contains(new ThreadData(23));
       // ExecutorService service = Executors.newCachedThreadPool(1,2,10, TimeUnit.SECONDS,new LinkedBlockingQueue<>());
    }

}
class ThreadData{
    int a =10;
    ThreadData(){}
    public ThreadData(int a){
        this.a = a;
    }
}

 enum  Week{
    A(1),V(2),B(4);
    private int realDay;
    Week(int realDay){

    }

}