package com.zhou.io;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class AIO {
    public static ThreadPoolExecutor poolExecutor = new ThreadPoolExecutor(2, Runtime.getRuntime().availableProcessors() * 2, 0, TimeUnit.MILLISECONDS, new LinkedBlockingDeque<Runnable>());

    public static void main(String[] args) {
        //bioServerStart();
        System.out.println();
        FileOutputStream outputStream = new FileOutputStream(FileDescriptor.out);
        try {
            outputStream.write('A');
            outputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private static void nioServerStart() throws IOException {
        Selector selector =Selector.open();
        ServerSocketChannel channel = ServerSocketChannel.open();
        channel.socket().bind(new InetSocketAddress(8081));
        channel.configureBlocking(false);
        SelectionKey s  = channel.register(selector,SelectionKey.OP_ACCEPT);
        while(true){

        }

    }
    private static void bioServerStart() {
        try {
            ServerSocket serverSocket = new ServerSocket(8081);
            while (true) {
                Socket accept = serverSocket.accept();
                poolExecutor.execute(new ServerHandler(accept));
            }
//            ServerSocket ss = new ServerSocket(8081);
//            System.out.println("启动服务器....");
//            Socket s = ss.accept();
//            System.out.println("客户端:"+s.getInetAddress().getLocalHost()+"已连接到服务器");
//
//            BufferedReader br = new BufferedReader(new InputStreamReader(s.getInputStream()));
//            //读取客户端发送来的消息
//            String mess = br.readLine();
//            System.out.println("客户端："+mess);
//            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(s.getOutputStream()));
//            bw.write(mess+"\n");
//            bw.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

class ServerHandler implements Runnable {

    private Socket socket;

    public ServerHandler(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            String readline = null;
            while ((readline  = reader.readLine()) !=null) {
                System.out.println("============" + readline);
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
                writer.write("收到 over" + "\n" );
                writer.flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        finally {
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}
