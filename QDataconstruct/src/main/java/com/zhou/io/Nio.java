package com.zhou.io;

import com.sun.security.ntlm.Server;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.util.Iterator;

/***
 * NIO 三大件: Selector,Channel,Buffer
 *http://zhhphappy.iteye.com/blog/2032893
 */
public class Nio {
    public static void main(String[] args) {
        try {
            serverStart();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @throws IOException
     */
    public static void serverStart() throws IOException {
        //channel bytebuffer selector 三兄弟
        ServerSocketChannel channel = ServerSocketChannel.open();
        channel.socket().bind(new InetSocketAddress(8087));
        channel.configureBlocking(false);
        Selector selector = Selector.open();
        channel.register(selector, SelectionKey.OP_ACCEPT);
        while (true) {
            int select = selector.select();
            if (select == 0) {
                continue;
            }
            System.out.println(111);
            Iterator<SelectionKey> iterators = selector.selectedKeys().iterator();
            while (iterators.hasNext()) {
                SelectionKey selectionKey = iterators.next();
                //accept
                if (selectionKey.isAcceptable()) {
                    SocketChannel socket = ((ServerSocketChannel) selectionKey.channel()).accept();
                    socket.register(selector, SelectionKey.OP_READ);
                } else if (selectionKey.isReadable()) {
                    SocketChannel socket = ((ServerSocketChannel) selectionKey.channel()).accept();
                    socket.register(selector, SelectionKey.OP_WRITE);
                } else {

                }

                iterators.remove();
                //read
                //write

            }
        }
    }

    public void channelCopy() throws IOException {
        ReadableByteChannel readableByteChannel = Channels.newChannel(System.in);
        WritableByteChannel writableByteChannel = Channels.newChannel(System.out);

        ByteBuffer buffer = ByteBuffer.allocate(16 * 1024);
        while (readableByteChannel.read(buffer) != -1) {
            buffer.flip();
            writableByteChannel.write(buffer);
            buffer.compact();
        }

        ByteBuffer header = ByteBuffer.allocate(10);
        ByteBuffer body = ByteBuffer.allocate(80);

        ByteBuffer[] byteBuffers = {header, body};
        //int bytesRead = readableByteChannel.read(byteBuffers);

    }
}
