package com.zhou.io;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.Scanner;

public class AIOClient {
    public static void main(String[] args) {
        bioClientStart();
    }

    private static void bioClientStart() {
        try {
            Socket socket = new Socket("127.0.0.1", 8087);
            InputStream inputStream = socket.getInputStream();
            OutputStream outputStream = socket.getOutputStream();
//
//            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream));
//            while (true) {
//                Scanner scanner = new Scanner(System.in);
//                if (scanner.hasNext()) {
//                    String line = scanner.nextLine();
//                    writer.write("werfsfdfdgdfd" + "/n");
//                    writer.flush();
//                    System.out.println(line);
//                    if (line.equals("end")) {
//                        break;
//                    }
//                }
//            }
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(outputStream));
            //向服务器端发送一条消息
            bw.write("测试客户端和服务器通信，服务器接收到消息返回到客户端\n");
            bw.flush();

            //读取服务器返回的消息
            BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
            String mess = br.readLine();
            System.out.println("服务器："+ mess);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
