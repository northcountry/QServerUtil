package com.zhou.juc;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicStampedReference;

public class AtomicDemo {
    private static AtomicInteger integer = new AtomicInteger(1);
    private static volatile int  num = 0;
    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            Thread t = new Thread(new MyRun(integer,num),"thread - " + i);
            t.start();
        }
    }
    //AtomicStampedReference atomicStampedReference = new AtomicStampedReference(1);

}

class MyRun implements Runnable {
    AtomicInteger integer;
    int  num;
    public MyRun(AtomicInteger integer,int num) {
        this.integer = integer;
        this.num = num;
    }

    @Override
    public void run() {
        for (int i = 0; i < 200; i++) {
            num ++;
            //System.out.println(Thread.currentThread().getName() + ":" + num);
            integer.getAndIncrement();
           System.out.println(Thread.currentThread().getName() + ":" + integer);
        }
    }
}
