package com.zhou.juc;

import com.sun.corba.se.impl.orbutil.concurrent.Mutex;

import java.util.*;
import java.util.concurrent.*;

public class ThreadPoolDemo {
    public static void main(String[] args) {
        System.out.println(1);
        System.out.println(Thread.currentThread().getThreadGroup().getName());
        ThreadGroup group1 = new ThreadGroup("g1");

        ThreadGroup group2 = new ThreadGroup("g2");

        Thread t1 = new Thread(group1, "g1's member");
        Thread t2 = new Thread(group2, "g2's member");
        for (int i = 0; i < 10000; i++) {
            new Thread("thread:" + i).start();
        }

        ThreadPoolExecutor poolExecutor = new ThreadPoolExecutor(1, 2, 33, TimeUnit.MILLISECONDS, new LinkedBlockingDeque<Runnable>(1024));
        poolExecutor.execute(t1);
        poolExecutor.execute(t2);
        poolExecutor.shutdown();
        // poolExecutor.execute(t1);

        ThreadPoolExecutor pool = new ThreadPoolExecutor(1, 1, 3, TimeUnit.MILLISECONDS, new ArrayBlockingQueue<Runnable>(100));

        int count_bits = Integer.SIZE - 3;
        int CAPACITY = 0;
        System.out.println(-1 << count_bits);
        System.out.println(0 << count_bits);
        System.out.println((CAPACITY = 1 << count_bits) - 1);
        System.out.println(1 << count_bits);
        System.out.println(2 << count_bits);
        System.out.println(CAPACITY);
        System.out.println(~CAPACITY);

        Set<Integer> sets = new HashSet<>();

        sets.add(222);

        Iterator<Integer> iter = sets.iterator();
        while (iter.hasNext()){
            iter.next();
        }

        Hashtable hashtable =new Hashtable();

        Map p = new ConcurrentHashMap();
        p.put( 1,1);

        String s = new String();
//        new Thread(()->{
//            System.out.println(111);
//        }).start();

        ExecutorService executorService =  Executors.newCachedThreadPool();
        Executors.newSingleThreadExecutor();
        Executors.newFixedThreadPool(10);
        Executors.newScheduledThreadPool(12);
    }
}
