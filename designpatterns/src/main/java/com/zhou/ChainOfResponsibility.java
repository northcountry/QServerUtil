package com.zhou;

/**
 * 责任链 模式
 * 一系列得类去处理相同得请求
 * (1)web过滤器
 * (2)处理请假流程,3天以内组长批,7天以内项目经理批,以后就是老板批
 */
public class ChainOfResponsibility {
    public static void main(String[] args) {
        System.out.println("责任链模式 开启");
        Handler leader = new Leader("领导");
        Handler groupBoss = new GroupBoss("boss");
        Handler ceo = new CEO("ceo");
        leader.setHandler(groupBoss);
        groupBoss.setHandler(ceo);
        AskForLeave askForLeave = new AskForLeave(10);
        leader.processHandler(askForLeave);
    }
}

/**
 * 请假处理对象
 */
class AskForLeave{
    public  AskForLeave(int day){
        this.day = day;
    }
    private int day;
    public int getDay() {
        return day;
    }
    public void setDay(int day) {
        this.day = day;
    }

}
/**
 * 定义相同得处理方法
 */
abstract class Handler {
    Handler next;
    private String name;
    public  Handler(String name){
        this.name = name;
    }
    public void setHandler(Handler next) {
        this.next = next;
    }
    abstract void processHandler(AskForLeave askForLeave);
}

/**
 * 直属上级
 */
class Leader extends Handler{
    public Leader(String name) {
        super(name);
    }

    @Override
    void processHandler(AskForLeave askForLeave) {
        if (askForLeave.getDay()  < 3){
            System.out.println("approval");
        }else{
            System.out.println("权限不够  继续走");
            this.next.processHandler(askForLeave);
        }
    }
}
class GroupBoss extends  Handler{
    public GroupBoss(String name) {
        super(name);
    }

    @Override
    void processHandler(AskForLeave askForLeave) {
        if (askForLeave.getDay()  >= 3 && askForLeave.getDay()  < 10){
            System.out.println("approval");
        }else{
            System.out.println("权限不够  继续走");
            this.next.processHandler(askForLeave);
        }
    }
}

class  CEO extends  Handler{
    public CEO(String name) {
        super(name);
    }

    @Override
    void processHandler(AskForLeave askForLeave) {
        System.out.println("Down");
    }
}